from sys import argv, maxsize
from random import uniform

import numpy
from PIL import Image, ImageDraw


def main():
    command = argv[1]
    filename = argv[3]

    if command == 'heightmap':
        depth = int(argv[2])
        generate_heightmap(depth, filename)
    elif command == 'normal':
        heightmap_path = argv[2]
        generate_normal_map(heightmap_path, filename)


def generate_heightmap(depth, filename):
    width = height = pow(2, depth) + 1
    matrix = [[0 for x in range(width)] for y in range(height)]

    seed = 400
    seed_corners(seed, matrix, width, height)

    step_size = width - 1
    half_step_size = step_size//2

    offset = 250

    while step_size > 1:
        for x in range(0, width-1, step_size):
            for y in range(0, height-1, step_size):
                diamond_step(matrix, x, y, step_size, offset)

        for x in range(0, width, half_step_size):
            for y in range((x+half_step_size) % step_size, height, step_size):
                square_step(matrix, x, y, half_step_size, offset)

        step_size = step_size//2
        half_step_size = step_size//2
        offset = offset//2

    im = Image.new('RGBA', (width, height), (0, 0, 0, 255))

    draw_pixels(im, matrix, width, height)

    ImageDraw.ImageDraw(im)

    im.save(filename, 'PNG')


def seed_corners(seed, matrix, width, height):
    matrix[0][0] = seed
    matrix[width - 1][0] = seed
    matrix[width - 1][height - 1] = seed
    matrix[0][height - 1] = seed


def diamond_step(matrix, x, y, step_size, offset):

    positions = [matrix[x][y],
                 matrix[x + step_size][y],
                 matrix[x + step_size][y + step_size],
                 matrix[x][y + step_size]]

    avg = average(positions)
    r = uniform(0, 1)

    half_step_size = step_size//2
    matrix[x + half_step_size][y + half_step_size] = avg + (r*2*offset) - offset


def square_step(matrix, x, y, half_step_size, offset):
    positions = [matrix[(x - half_step_size + len(matrix[0]) - 1) % (len(matrix[0]) - 1)][y],
                 matrix[x][(y - half_step_size + len(matrix[0]) - 1) % (len(matrix[0]) - 1)],
                 matrix[(x + half_step_size) % (len(matrix[0]) - 1)][y],
                 matrix[x][(y + half_step_size) % (len(matrix[0]) - 1)]]

    avg = average(positions)
    r = uniform(0, 1)
    matrix[x][y] = avg + (r*2*offset) - offset


def average(l):
    return sum(l) / float(len(l))


def get_lowest_value(matrix, width, height):
    minimum = maxsize
    for x in range(0, width):
        for y in range(0, height):
            if matrix[x][y] < minimum:
                minimum = matrix[x][y]

    return minimum


def get_highest_value(matrix, width, height):
    maximum = -maxsize
    for x in range(0, width):
        for y in range(0, height):
            if matrix[x][y] > maximum:
                maximum = matrix[x][y]

    return maximum


def draw_pixels(image, matrix, width, height):
    lowest_value = get_lowest_value(matrix, width, height)
    highest_value = get_highest_value(matrix, width, height)

    px = image.load()

    for x in range(0, width):
        for y in range(0, height):
            value = float_to_rgb(lowest_value, highest_value, matrix[x][y])
            px[x, y] = (value, value, value)


def float_to_rgb(lowest_value, highest_value, floor_delta):
    total_delta = highest_value - lowest_value
    floor_delta = floor_delta - lowest_value

    return int(floor_delta / total_delta * 255)


def generate_normal_map(heightmap_path, filename):
    heightmap_array = numpy.asarray(Image.open(heightmap_path))
    heightmap_array_length = len(heightmap_array)

    normal_map_array = []
    for x in range(heightmap_array_length):
        line = []
        for y in range(heightmap_array_length):
            vec = extract_3d_vector(x, y, heightmap_array)
            value = [int(vec.x * 255), int(vec.y * 255), int(vec.z * 255), ]
            line.append(value)
        normal_map_array.append(line)

    im = Image.new('RGBA', (heightmap_array_length, heightmap_array_length), (0, 0, 0, 255))

    draw_colored_pixels(im, normal_map_array, heightmap_array_length, heightmap_array_length)
    im.save(filename, 'PNG')


def draw_colored_pixels(image, matrix, width, height):
    px = image.load()
    for x in range(0, width):
        for y in range(0, height):
            px[x, y] = (matrix[x][y][0], matrix[x][y][1], matrix[x][y][2])


def extract_3d_vector(x, y, array):
    array_length = len(array)-1
    scale = numpy.log2(array_length)/len(array)

    top_left = int(array[(x-1) % array_length][(y-1) % array_length][0])
    top_center = int(array[x][(y-1) % array_length][0])
    top_right = int(array[(x+1) % array_length][(y-1) % array_length][0])
    center_left = int(array[(x-1) % array_length][y][0])
    center_right = int(array[(x+1) % array_length][y][0])
    bottom_left = int(array[(x-1) % array_length][(y+1) % array_length][0])
    bottom_center = int(array[x][(y+1) % array_length][0])
    bottom_right = int(array[(x+1) % array_length][(y+1) % array_length][0])

    n = Vector3D(0, 0, 0)

    n.x = scale * -(bottom_right - bottom_left + 2*(center_right - center_left) + top_right - top_left)
    n.y = scale * -(top_left - bottom_left + 2*(top_center - bottom_center) + top_right - bottom_right)
    n.z = 1

    n.normalize()
    n = rescale(n)
    return n


def rescale(vec):
    return Vector3D((vec.x+1) / 2, (vec.y+1) / 2, (vec.z+1) / 2)


class Vector3D(object):

    def __init__(self, x, y, z):
        r"""
        Creates an instance of 3D vector.

        >>> u = Vector3D(1,-2,3)
        """
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> Vector3D(-2,3,5)
        Vector3D(-2,3,5)
        """
        return 'Vector3D({},{},{})'.format(self.x, self.y, self.z)

    def norm(self):
        r"""
        Returns the norm of self.

        >>> is_close(Vector3D(0,1,0).norm(), 1.0)
        True
        >>> is_close(Vector3D(1,1,0).norm(), 1.4142)
        True
        """
        norm = numpy.sqrt(pow(self.x, 2) +
                          pow(self.y, 2) +
                          pow(self.z, 2))
        return norm

    def normalize(self):
        r"""
        Normalizes self.

        Given a vector `v`, its associated normalized vector is the unit vector
        having the same direction.

        >>> u = Vector3D(1,1,1)
        >>> u.normalize()
        >>> is_close(u.norm(), 1.0)
        True
        """
        norm = self.norm()
        self.x = self.x/norm
        self.y = self.y/norm
        self.z = self.z/norm
        return


if __name__ == '__main__':
    main()
