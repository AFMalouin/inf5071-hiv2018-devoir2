// Question 2.1
#define PI 3.14159265
#define A 20.0
#define SPEED 20.0

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 center = iResolution.xy / 2.0;
    vec2 dist = center - fragCoord;
    float r = length(dist);
    float theta = atan(dist.y, dist.x) / (2.0 * PI);
    r = (r - A * theta) - (iTime * SPEED);
    float c = r / A - (floor(r / A));
    fragColor = vec4(smoothstep(0.2, 0.0, abs(c - 0.25)));
}

// Question 2.2
#define PI 3.14159265
#define A 10.0
#define SPEED 20.0

#define RED vec4(1, 0, 0, 1)
#define GREEN vec4(0, 1, 0, 1)
#define BLUE vec4(0,0,1,1);

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 center = iResolution.xy / 2.0;
    vec2 dist = center - fragCoord;
    float r = length(dist);
    float theta = atan(dist.y, dist.x) / (2.0 * PI);
    float A4 = A * 4.0;
    float r1 = (r - A4 * theta)- (iTime * SPEED);
    
    vec4 color = vec4(0,0,0,1);
    
    float v = fract(r1/A4);
    
    if (v <= 0.25) {
    	color = RED;
    }
    if (v > 0.25 && v <= 0.5) {
    	color = BLUE;
    }
    if (v > 0.5 && v <= 0.75) {
    	color = GREEN;
    }
    
    theta = theta * 4.0;
    r = (r - A * theta)- (iTime * SPEED);
    float c = fract(r / A);
    
    fragColor = vec4(smoothstep(0.2, 0.0, abs(c - 0.25)))*color;
}