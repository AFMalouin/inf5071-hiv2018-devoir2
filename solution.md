# Solution au devoir 2

## Auteur

Arnaud Foulem Malouin
FOUA28049301

## Solution à la question 1

Les solutions aux sous-questions 1.1 et 1.2 peuvent êtres consultées dans le fichier [q1.py](q1.py).

###  Solution à la sous-question 1.1

J'ai opté pour implémenter un algoritme générant des textures répétables afin d'obtenir un meilleur rendu 
lors de la génération des cartes de normales. Ça a occasionné de légers "pincements" sur les rebords
de l'image mais évite d'avoir des rangées de pixels aberrants sur les 4 rebords de mes cartes de normales.

#### Exemple d'exécution

La commande
```
python q1.py heightmap 8 heightmap.png
```
produit une texture carrée de dimension $`2^8 + 1 \times 2^8 + 1`$  avec niveau de gris et 
sauvegarde le résultat dans le fichier `heightmap.png`. Voici quelques exemples de textures
produites avec _ma_ solution (disponibles dans le dossier `exemples` de ce
dépôt):

| profondeur | image                        |
| :--------: | :--------------------------: |
| 2          | ![](exemples/heightmap2.png) |
| 3          | ![](exemples/heightmap3.png) |
| 4          | ![](exemples/heightmap4.png) |
| 5          | ![](exemples/heightmap5.png) |
| 6          | ![](exemples/heightmap6.png) |
| 7          | ![](exemples/heightmap7.png) |
| 8          | ![](exemples/heightmap8.png) |
| 9          | ![](exemples/heightmap9.png) |

###  Solution à la sous-question 1.2

#### Explications

J'ai adapté la solution décrite dans cette [réponse de StackOverflow](https://stackoverflow.com/a/26357357/1060988).

#### Exemple d'exécution

La commande
```
python q1.py normal heightmap.png normal_map.png
```
produit une carte de normales et la sauvegarde dans le fichier
`normal_map.png` à partir de la carte de hauteurs données dans le fichier
`heightmap.png`. Voici quelques exemples de textures
produites avec _ma_ solution (disponibles dans le dossier `exemples` de ce
dépôt):

| profondeur | image                     |
| :--------: | :-----------------------: |
| 2          | ![](exemples/normal2.png) |
| 3          | ![](exemples/normal3.png) |
| 4          | ![](exemples/normal4.png) |
| 5          | ![](exemples/normal5.png) |
| 6          | ![](exemples/normal6.png) |
| 7          | ![](exemples/normal7.png) |
| 8          | ![](exemples/normal8.png) |
| 9          | ![](exemples/normal9.png) |

## Solution à la question 2

Les solutions aux sous-questions 2.1 et 2.2 peuvent êtres consultées dans le fichier [q2.txt](q2.txt).

###  Solution à la sous-question 1.1

#### Code

```glsl
// Question 2.1
#define PI 3.14159265
#define A 20.0
#define SPEED 20.0

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 center = iResolution.xy / 2.0;
    vec2 dist = center - fragCoord;
    float r = length(dist);
    float theta = atan(dist.y, dist.x) / (2.0 * PI);
    r = (r - A * theta) - (iTime * SPEED);
    float c = r / A - (floor(r / A));
    fragColor = vec4(smoothstep(0.2, 0.0, abs(c - 0.25)));
}
```

#### Explications

Tel que suggéré sur la (page wikipedia de la spirale d'Archimède)[https://en.wikipedia.org/wiki/Archimedean_spiral], 
j'ai fait varier le paramètre $`a`$ dans la formule $`r=a+b*theta`$ avec le paramètre `iTime` d'OpenGL.

#### Exemple

Voici un exemple du résultat produit avec _ma_ solution (disponible dans le dossier `movies` de ce
dépôt):

![](movies/animated_spiral.webm)

###  Solution à la sous-question 1.2

#### Code

```glsl
// Question 2.2
#define PI 3.14159265
#define A 10.0
#define SPEED 20.0

#define RED vec4(1, 0, 0, 1)
#define GREEN vec4(0, 1, 0, 1)
#define BLUE vec4(0,0,1,1);

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 center = iResolution.xy / 2.0;
    vec2 dist = center - fragCoord;
    float r = length(dist);
    float theta = atan(dist.y, dist.x) / (2.0 * PI);
    float A4 = A * 4.0;
    float r1 = (r - A4 * theta)- (iTime * SPEED);
    
    vec4 color = vec4(0,0,0,1);
    
    float v = fract(r1/A4);
    
    if (v <= 0.25) {
    	color = RED;
    }
    if (v > 0.25 && v <= 0.5) {
    	color = BLUE;
    }
    if (v > 0.5 && v <= 0.75) {
    	color = GREEN;
    }
    
    theta = theta * 4.0;
    r = (r - A * theta)- (iTime * SPEED);
    float c = fract(r / A);
    
    fragColor = vec4(smoothstep(0.2, 0.0, abs(c - 0.25)))*color;
}
```

#### Explications

J'ai fait varier la couleur du pixel sortant de la fonction `smoothstep` selon la partie
fractionnelle de la variable `c`.

#### Exemple

Voici un exemple du résultat produit avec _ma_ solution (disponible dans le dossier `movies` de ce
dépôt):

![](movies/animated_colored_spirals.webm)


## Dépendances

### Question 1

- [Python 3.6](https://www.python.org/downloads/)
- [Pillow](https://pillow.readthedocs.io/en/latest/)
- [NumPy](http://www.numpy.org/)

### Question 2

- Le code roule sur l'éditeur en ligne de [Shadertoy](https://www.shadertoy.com)

## Références

### Sous-question 1.1

- [Calcul du décalage aléatoire (avg + (r * 2 * offset) - offset), valeur de départ de y (ligne 43) et astuce pour obtenir une texture répétable.](https://stackoverflow.com/questions/2755750/diamond-square-algorithm)


- [Inspiration pour la structure de la boucle de generate_heightmap.](http://jmecom.github.io/blog/2015/diamond-square/)

### Sous-question 1.2

- Utilisation  de ma classe Vecteur3D écrite pour le devoir 1.

- [Solution basé sur la façon décrite dans cette réponse de StackOverflow.](https://stackoverflow.com/a/26357357/1060988)

### Sous-question 2.2

- [Inspiration pour la coloration des spirales à l'aide de la fonction fract d'OpenGL.](https://www.shadertoy.com/view/XtByRz)

### solution.md
La structure et la présentation de ce fichier `solution.md` sont inspirées du [README](README.md) fourni par Alexandre Blondin Massé.

## État du devoir

Le devoir est complété et tout fonctionne correctement à priori.